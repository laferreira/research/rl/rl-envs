if __name__ == '__main__':
    import sys
    from environment import Environment

    address = '*'
    port = 5555

    print("Using configuration")
    print("Address: {}".format(address))
    print("Port: {}".format(port), end='\n\n')

    Environment(address, port).run_environment()
