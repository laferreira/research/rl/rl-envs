from abc import ABCMeta, abstractmethod


# -----------------------------------------------------------------------------
class MetaEnvironment(metaclass=ABCMeta):

    import zmq

    # -------------------------------------------------------------------------
    # Communication related methods
    # -------------------------------------------------------------------------
    def __init__(self, address, port):
        add = "tcp://{}:{}".format(address, port)
        self.socket = self.zmq.Context().socket(self.zmq.REP)
        self.socket.bind(add)

    # -------------------------------------------------------------------------
    def send_data(self, data):
        self.socket.send_json(data)

    # -------------------------------------------------------------------------
    def recv_data(self):
        return self.socket.recv_json()

    # -------------------------------------------------------------------------
    def format_msg(self, terminal, state, actions, reward):
        return {'terminal': terminal,
                'state': state,
                'actions': actions,
                'reward': reward
                }

    # -------------------------------------------------------------------------
    # Agent-Environment interaction related methods
    # -------------------------------------------------------------------------
    def run_environment(self):
        while True:
            agent_msg = self.recv_data()

            if agent_msg['init'] == True:
                msg = self.init()
            else:
                msg = self.run(agent_msg)

            self.send_data(msg)

        self.socket.close()

    # -------------------------------------------------------------------------
    @abstractmethod
    def init(self):
        """ Method called when the interaction with the agent starts. """
        pass

    # -------------------------------------------------------------------------
    @abstractmethod
    def run(self, agent_msg):
        """ Method called when interacting with the environment.  """
        pass

    # -------------------------------------------------------------------------

# -----------------------------------------------------------------------------
