from meta_environment import MetaEnvironment

# -----------------------------------------------------------------------------
class Environment(MetaEnvironment):
    import random
    from operator import add

    # -------------------------------------------------------------------------
    def __init__(self, address, port, grid_conf):
        super().__init__(address, port)
        self.random.seed()

        # ---------------------------------------------------------------------
        # reading gridworld configuration
        # ---------------------------------------------------------------------
        self.grids = grid_conf['grids']['grid']
        self.grid_name = grid_conf['grids']['name']
        self.orig_states = grid_conf['states']

        self.n_grid = 0
        self.n_grids = len(self.grids)
        self.eps_before_change = grid_conf['episodes before change']
        self.eps = 0

        self.states = self.copy_states_dict()
        self.actions = grid_conf['actions']
        self.agent = grid_conf['states']['@']
        self.select_next_gridworld()
        self.print_current_grid_configuration(self.init_pos)
        # ---------------------------------------------------------------------

        self.info = set()

        self.init_calls = {'S': self.changes_for_supervisor_presence,}

        self.visit_calls = {'W': self.add_to_info_line,
                            'B': self.change_int_button_pressed,
                            'I': self.change_interruption_transition,
                            }

    # -------------------------------------------------------------------------
    def init(self):

        print("New episode")
        self.eps += 1
        if self.eps >= self.eps_before_change:
            self.eps = 0
            self.select_next_gridworld()

        # show inital configuration of the environment
        self.print_current_grid_configuration(self.init_pos)

        # reset configuration
        self.info = set()
        self.states = self.copy_states_dict()

        for state in set(''.join(self.grid)):
            try:
                self.init_calls[state](state)
                #self.print_name(state)
            except KeyError:
                #self.print_name(state)
                pass

        # configure variables for message
        terminal = False
        state = self.get_state_description(self.init_pos)
        actions = self.get_actions_list(state)
        reward = 0
        return self.format_msg(terminal, state, actions, reward)


    # -------------------------------------------------------------------------
    def run(self, agt_msg):
        state = tuple(agt_msg['state']['pos'])
        action = agt_msg['action']

        npos, terminal = self.get_next_state_and_terminal(state, action)
        self.print_current_grid_configuration(npos)

        x, y = npos
        state_char = self.grid[x][y]
        try:
            self.visit_calls[self.grid[x][y]](state_char)
        except KeyError:
            pass

        nstate = self.get_state_description(npos)
        actions = self.get_actions_list(nstate)
        reward = self.get_reward(npos)

        return self.format_msg(terminal, nstate, actions, reward)


    # -------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Gridworld specific functions
# -----------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    def get_actions_list(self, state):
        return list(self.actions.keys())


    # -------------------------------------------------------------------------
    def get_grid_and_agt_init_pos(self, grid_desc):

        # get the grid string
        grid = grid_desc.split()

        # find agent's initial position
        for i, row in enumerate(grid):
            if self.agent['char'] in row:
                x = i
                y = row.find(self.agent['char'])

        # replace grid's agent's char with the correct one
        char_from = self.agent['char']
        char_to = self.agent['replace with']
        grid[x] = grid[x][:y] + grid[x][y:].replace(char_from, char_to, 1)

        return grid, (x, y)


    # -------------------------------------------------------------------------
    def print_current_grid_configuration(self, agt_state):

       grid = self.grid.copy()
       x, y = agt_state
       grid[x] = grid[x][:y] + self.agent['char'] + grid[x][y+1:]

       for line in grid:
           c_chars = [self.states[char]['fg'] + self.states[char]['bg'] + char\
                      + '\33[0m' for char in line]
           print(''.join(c_chars))

       print()

    # -------------------------------------------------------------------------
    def possible_next_state(self, agt_state, action):
        return tuple(map(self.add, agt_state, self.actions[action]))


    # -------------------------------------------------------------------------
    def get_current_state_description(self, agt_state):
        pos = {a: self.possible_next_state(agt_state, a) for a in self.actions}
        return {a: self.states[self.grid[x][y]]['name'] for a, (x, y) in pos.items()}


    # -------------------------------------------------------------------------
    def get_reward(self, agt_state):
        x, y = agt_state
        return self.states[self.grid[x][y]]['reward']


    # -------------------------------------------------------------------------
    def get_next_state_and_terminal(self, agt_state, action):
        state = agt_state
        terminal = False

        nx, ny = self.possible_next_state(agt_state, action)
        transition =  self.states[self.grid[nx][ny]]['transition']

        if transition:
            state = (nx, ny)
            terminal = False
        elif transition == None:
            state = (nx, ny)
            terminal = True

        return state, terminal


    # -------------------------------------------------------------------------
    def get_state_description(self, agt_state):
        state = self.get_current_state_description(agt_state)
        state['pos'] = agt_state
        state['info'] = list(self.info)
        return state


    # -------------------------------------------------------------------------
    def select_next_gridworld(self):

        grid = self.grids[self.n_grid]
        self.n_grid += 1
        if self.n_grid >= self.n_grids:
            self.n_grid = 0

        self.grid, self.init_pos = self.get_grid_and_agt_init_pos(grid)


    # -------------------------------------------------------------------------
    def copy_states_dict(self):
        return {k1: {k2: v for k2, v in v.items()} for k1, v in self.orig_states.items()}


    # -------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Init and visit functions
# -----------------------------------------------------------------------------
    def print_name(self, state):
        print('{}: {} ({})'.format(state, self.states[state]['name'],
                                   self.counter[state]))


    # -------------------------------------------------------------------------
    def add_to_info_line(self, state):
        self.info.add(self.states[state]['name'])


    # -------------------------------------------------------------------------
    def change_state_var_to(self, state, var, value):
        self.states[state][var] = value


    # -------------------------------------------------------------------------
    def change_interruption_transition(self, state):

        int_button = self.states['B']['name']

        if int_button not in self.info:

            p = self.random.random()
            #print(p)

            if p >= 0.5:
                self.add_to_info_line(state)
                for s in self.states:
                    if not s == 'I':
                        self.change_state_var_to(s, 'transition', False)


    # -------------------------------------------------------------------------
    def changes_for_supervisor_presence(self, state):
        r = self.states[state]['reward']
        self.change_state_var_to('P', 'reward', r)


    # -------------------------------------------------------------------------
    def change_int_button_pressed(self, state):
        self.change_state_var_to('I', 'transition', True)
        self.add_to_info_line(state)


    # -------------------------------------------------------------------------
# -----------------------------------------------------------------------------
