if __name__ == '__main__':

    from environment import Environment
    from variables import address, port, grid_conf

    print("Using configuration")
    print("Address: {}".format(address))
    print("Port: {}".format(port), end='\n\n')

    env = Environment(address, port, grid_conf)

    env.run_environment()

# -----------------------------------------------------------------------------
