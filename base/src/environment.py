from meta_environment import MetaEnvironment

# -----------------------------------------------------------------------------
class Environment(MetaEnvironment):
    import random

    # -------------------------------------------------------------------------
    def __init__(self, address, port):
        super().__init__(address, port)
        self.random.seed()
        self.actions = ['a0', 'a1', 'a2', 'a3']

    # -------------------------------------------------------------------------
    def init(self):
        return self.format_msg(False, 's0', self.actions , -1)

    # -------------------------------------------------------------------------
    def run(self, agent_msg):
        terminal = False
        state = 's{}'.format(self.random.randint(1, 5))
        actions = self.actions
        reward = self.random.uniform(0, 100)

        return self.format_msg(terminal, state, actions, reward)

    # -------------------------------------------------------------------------
# -----------------------------------------------------------------------------
