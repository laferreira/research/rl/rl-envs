from abc import ABCMeta, abstractmethod
from typing import Dict, List


# -----------------------------------------------------------------------------
class MetaEnvironment(metaclass=ABCMeta):

    import zmq

    # -------------------------------------------------------------------------
    # Communication related methods
    # -------------------------------------------------------------------------
    def __init__(self, address: str, port: int) -> None:
        add = "tcp://{}:{}".format(address, port)
        self.socket = self.zmq.Context().socket(self.zmq.REP)
        self.socket.bind(add)

    # -------------------------------------------------------------------------
    def send_data(self, data: Dict) -> None:
        self.socket.send_json(data)

    # -------------------------------------------------------------------------
    def recv_data(self) -> Dict:
        return self.socket.recv_json()

    # -------------------------------------------------------------------------
    def format_msg(self, terminal: bool,
                   state,
                   actions: List,
                   reward: float) -> Dict:
        return {'terminal': terminal,
                'state': state,
                'actions': actions,
                'reward': reward
                }

    # -------------------------------------------------------------------------
    # Agent-Environment interaction related methods
    # -------------------------------------------------------------------------
    def run_environment(self) -> None:
        while True:
            agent_msg = self.recv_data()

            if agent_msg['init'] == True:
                msg = self.init()
            else:
                msg = self.run(agent_msg)

            self.send_data(msg)

        self.socket.close()

    # -------------------------------------------------------------------------
    @abstractmethod
    def init(self) -> None:
        """ Method called when the interaction with the agent starts. """
        pass

    # -------------------------------------------------------------------------
    @abstractmethod
    def run(self, agent_msg: Dict) -> None:
        """ Method called when interacting with the environment.  """
        pass

    # -------------------------------------------------------------------------

# -----------------------------------------------------------------------------
